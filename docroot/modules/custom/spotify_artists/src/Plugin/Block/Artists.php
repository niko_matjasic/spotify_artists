<?php

namespace Drupal\spotify_artists\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\CacheTagsInvalidatorInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\spotify_artists\SpotifyApiServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides an 'Artists' block.
 *
 * @Block(
 *   id = "artists_block",
 *   admin_label = @Translation("Artists"),
 * )
 */
class Artists extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Spotify api variable.
   *
   * @var \Drupal\spotify_artists\SpotifyApiServiceInterface
   */
  protected $spotifyApi;

  /**
   * Cache tag invalidator variable.
   *
   * @var \Drupal\Core\Cache\CacheTagsInvalidatorInterface
   */
  protected $cacheTagsInvalidator;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, SpotifyApiServiceInterface $spotifyApi, CacheTagsInvalidatorInterface $cacheTagsInvalidator) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->spotifyApi = $spotifyApi;
    $this->cacheTagsInvalidator = $cacheTagsInvalidator;

  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('spotify_api.get_artists'),
      $container->get('cache_tags.invalidator')
    );

  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    $artists = $this->spotifyApi->getArtists();
    return [
      '#theme' => 'spotify-artists',
      '#artists' => $artists,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);
    $config = $this->getConfiguration();

    $form['artists_quantity'] = [
      '#type' => 'number',
      '#title' => $this->t('Quantity'),
      '#description' => $this->t('How many artists you want to be visible?'),
      '#min' => 1,
      '#max' => 20,
      '#default_value' => $config["artists_quantity"],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);
    // Spotify artist id's.
    $artists_ids = [
      '4nDoRrQiYLoBzwC5BhVJzF',
      '6LuN9FCkKOj5PcnpouEgny',
      '163tK9Wjr9P9DmM0AVK7lm',
      '4GvEc3ANtPPjt1ZJllr5Zl',
      '0NB5HROxc8dDBXpkIi1v3d',
      '4UFShyUQuA8dguoZrqX0jQ',
      '4gzpq5DPGxSnKTe4SA8HAU',
      '1dy5WNgIKQU6ezkpZs4y8z',
      '6g0mn3tzAds6aVeUYRsryU',
      '2YqJwmohaNjg9lg51flSax',
      '3WtrH1zNpzoPSz6XpwCh6y',
      '0B3N0ZINFWvizfa8bKiz4v',
      '0NIIxcxNHmOoyBx03SfTCD',
      '4FH6xGueB6CRmiEdtPnu41',
      '0eecdvMrqBftK0M1VKhaF4',
      '6jEiUoyyJNPHzSR0Nib6HX',
      '13dTrWNNrnZ3AkgNyQNKP5',
      '28dDyPkw069ypiqC78dhRb',
      '69761NObDw2KwmmFgZmxzC',
      '4pUwtnbS6FdBniLp410AOu',
    ];
    $values = $form_state->getValues();
    $this->configuration['artists_quantity'] = $values['artists_quantity'];
    if (!empty($values['artists_quantity'])) {
      $this->configuration['artists_ids'] = array_slice($artists_ids, 0, $values['artists_quantity']);
    }
    else {
      $this->configuration['artists_ids'] = array_slice($artists_ids, 0, 20);
    }
    $this->cacheTagsInvalidator->invalidateTags(['block_spotify_artists']);

  }

  /**
   * {@inheritdoc}
   */
  public function blockValidate($form, FormStateInterface $form_state) {
    if ($form_state->getValue('artists_quantity') > 20) {
      $form_state->setErrorByName('artists_quantity', $this->t('Maximum amount is 20 artists.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    return parent::getCacheTags() + ['block_spotify_artists'];
  }

}
