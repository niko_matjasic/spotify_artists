<?php

namespace Drupal\spotify_artists\Plugin\AuthenticationTypes;

/**
 * Briefing client flow credentials for API requests.
 *
 * @package Drupal\spotify_artists\Plugin\AuthenticationTypes
 */
class ClientFlowAuthentication implements AuthenticationInterface {

  /**
   * {@inheritdoc}
   */
  public function authCredentials() {
    $credentials_config = \Drupal::config('spotify_api_configuration.settings');

    return [
      'client_id' => $credentials_config->get('spotify_api_configuration.client_id'),
      'client_secret' => $credentials_config->get('spotify_api_configuration.client_secret'),
    ];
  }

}
