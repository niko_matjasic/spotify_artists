<?php

namespace Drupal\spotify_artists\Plugin\AuthenticationTypes;

/**
 * Authentications types interface.
 *
 * @package Drupal\spotify_artists\Plugin\AuthenticationTypes
 */
interface AuthenticationInterface {

  /**
   * Get client credentials from development config.
   *
   * @return array
   *   Brief client id and secret key.
   */
  public function authCredentials();

}
