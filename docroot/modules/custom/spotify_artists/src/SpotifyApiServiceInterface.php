<?php

namespace Drupal\spotify_artists;

use Drupal\spotify_artists\Plugin\AuthenticationTypes\AuthenticationInterface;

/**
 * Interface
 *
 * @package Drupal\spotify_artists
 */
interface SpotifyApiServiceInterface {

  /**
   * Get credentials from type.
   *
   * @param \Drupal\spotify_artists\Plugin\AuthenticationTypes\AuthenticationInterface $authentication
   *   Multiple types.
   *
   * @return array
   *   Return array with credentials.
   */
  public function getAuthentication(AuthenticationInterface $authentication);

  /**
   * Brief artists information.
   *
   * @return array|null
   *   Artists object.
   */
  public function getArtists();

  /**
   * Artist content.
   *
   * @param string $artist_id
   *   Artist ID per path.
   *
   * @return array
   *   Information.
   */
  public function getArtist($artist_id);

}
