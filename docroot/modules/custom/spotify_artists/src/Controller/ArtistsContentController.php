<?php

namespace Drupal\spotify_artists\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\spotify_artists\SpotifyApiServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provide artist information content.
 *
 * @package Drupal\spotify_artists\Controller
 */
class ArtistsContentController extends ControllerBase {

  /**
   * Spotify api service variable.
   *
   * @var \Drupal\spotify_artists\SpotifyApiService
   */
  protected $spotifyApi;

  /**
   * ArtistsContentController constructor.
   *
   * @param \Drupal\spotify_artists\SpotifyApiServiceInterface $spotifyApi
   *   SpotifyApi variable.
   */
  public function __construct(SpotifyApiServiceInterface $spotifyApi) {
    $this->spotifyApi = $spotifyApi;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('spotify_api.get_artists')
    );
  }

  /**
   * Brief single artist information.
   *
   * @param string $artist_id
   *   Artist ID.
   *
   * @return array
   *   Brief data to theme template.
   */
  public function index($artist_id) {
    $artist = $this->spotifyApi->getArtist($artist_id);

    return [
      '#theme' => 'artist-information',
      '#artist' => $artist,
      '#cache' => [
        'contexts' => ['url'],
      ],
    ];

  }

}
