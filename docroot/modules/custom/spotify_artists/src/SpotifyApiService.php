<?php

namespace Drupal\spotify_artists;

use Drupal\spotify_artists\Plugin\AuthenticationTypes\AuthenticationInterface;
use Drupal\spotify_artists\Plugin\AuthenticationTypes\ClientFlowAuthentication;
use GuzzleHttp\Client;
use Drupal\block\Entity\Block;

/**
 * Providing spotify API's.
 *
 * @package Drupal\spotify_artists
 */
class SpotifyApiService implements SpotifyApiServiceInterface {

  /**
   * Variable for GuzzleHTTP.
   *
   * @var \GuzzleHttp\Client
   */
  protected $http;

  /**
   * SpotifyApiService constructor.
   *
   * @param \GuzzleHttp\Client $http
   *   GuzzleHttp for API calls.
   */
  public function __construct(Client $http) {
    $this->http = $http;

  }

  /**
   * {@inheritdoc}
   */
  public function getAuthentication(AuthenticationInterface $authentication) {
    return $authentication->authCredentials();

  }

  /**
   * Format authorization base64.
   *
   * @param array $credentials
   *   Client creedentials.
   *
   * @return string
   *   Authentication for API.
   */
  private function formatBasicAuth(array $credentials) {
    $basicAuthCredential = $credentials['client_id'] . ":" . $credentials['client_secret'];
    $base64 = base64_encode($basicAuthCredential);
    return 'Basic ' . $base64;

  }

  /**
   * Authenticate with spotify API.
   */
  private function postRequestMethod() {
    $authorization = $this->formatBasicAuth($this->getAuthentication(new ClientFlowAuthentication()));
    if (!$authorization) {
      return NULL;
    }
    $url = 'https://accounts.spotify.com/api/token';
    $response = $this->http->post(
      $url, [
        'headers' => [
          'Content-Type' => 'application/x-www-form-urlencoded',
          'Authorization' => $authorization,
        ],
        'form_params' => [
          'grant_type' => 'client_credentials',
        ],
      ]
    );
    $body = $response->getBody()->getContents();
    $content = json_decode($body);
    if (isset($content->access_token)) {
      return $content->access_token;
    }

    return NULL;

  }

  /**
   * {@inheritdoc}
   */
  public function getArtists() {

    $access_token = $this->postRequestMethod();
    $artists_block = Block::load('artists');
    $artists_ids = '';
    if ($artists_block) {
      $configurations = $artists_block->get('settings');
      $artists_ids = $configurations['artists_ids'];
      if (isset($artists_ids)) {
        $artists_ids = implode(',', $artists_ids);
      }
      else {
        return NULL;
      }
    }
    $url = 'https://api.spotify.com/v1/artists?ids=' . $artists_ids;
    $response = $this->http->get(
      $url, [
        'headers' => [
          'Authorization' => 'Bearer ' . $access_token,
        ],
      ]
    );
    $body = $response->getBody()->getContents();
    if (!empty($body)) {
      return json_decode($body);
    }
    return NULL;

  }

  /**
   * {@inheritdoc}
   */
  public function getArtist($artist_id) {

    $access_token = $this->postRequestMethod();
    $url = 'https://api.spotify.com/v1/artists/' . $artist_id;
    $response = $this->http->get(
      $url, [
        'headers' => [
          'Authorization' => 'Bearer ' . $access_token,
        ],
      ]
    );
    $body = $response->getBody()->getContents();
    if (!empty($body)) {
      return json_decode($body);
    }
    return NULL;

  }

}
